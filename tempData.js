export default tempData = [
  {
    id: 1,
    name: "Plan a Trip",
    color: "#24A6D9",
    todo: [
      {
        title: "Book Flight",
        completed: false,
      },
      {
        title: "Password check",
        completed: true,
      },
      {
        title: "Reserve Hotel room",
        completed: true,
      },
      {
        title: "Pack Luggage",
        completed: false,
      },
    ],
  },
  {
    id: 2,
    name: "Errands",
    color: "#8022D9",
    todo: [
      {
        title: "Buy Milk",
        completed: false,
      },
      {
        title: "Check lamp",
        completed: true,
      },
      {
        title: "Reserve Car",
        completed: true,
      },
    ],
  },
];
