import firebase from "firebase";

import "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDpJ2AZxIM3R2LHekhw0VLp-mwYvGvSNGo",
  authDomain: "d-s-todo.firebaseapp.com",
  databaseURL: "https://d-s-todo.firebaseio.com",
  projectId: "d-s-todo",
  storageBucket: "d-s-todo.appspot.com",
  messagingSenderId: "951928321634",
  appId: "1:951928321634:web:05dbbc6f867a6c0884c33b",
  measurementId: "G-06RTCE59LV",
};

class Fire {
  constructor(callback) {
    this.init(callback);
  }
  init(callback) {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        callback(null, user);
      } else {
        firebase
          .auth()
          .signInAnonymously()
          .catch((error) => {
            callback(error);
          });
      }
    });
  }
  getLists(callback) {
    let ref = this.ref.orderBy("name");
    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];
      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });

      callback(lists);
    });
  }

  addList(list) {
    let ref = this.ref;

    ref.add(list);
  }

  updateList(list) {
    let ref = this.ref;
    ref.doc(list.id).update(list);
  }
  get userId() {
    return firebase.auth().currentUser.uid;
  }

  get ref() {
    return firebase
      .firestore()
      .collection("users")
      .doc(this.userId)
      .collection("lists");
  }

  detach() {
    this.unsubscribe();
  }
}

export default Fire;
