import React, { useState } from "react";

import { StyleSheet, Text, View, TouchableOpacity, Modal } from "react-native";
import colors from "../colors";
import TodoModal from "./TodoModal";

const TodoList = ({ list, updateList }) => {
  const [showList, setShowList] = useState(false);
  const ToggleList = () => {
    setShowList(!showList);
  };
  const completedCount = list.todo.filter((todo) => todo.completed).length;
  const remainingCount = list.todo.length - completedCount;
  return (
    <View>
      <Modal
        animationType="slide"
        visible={showList}
        onRequestClose={() => ToggleList()}
      >
        <TodoModal
          closeModal={() => ToggleList()}
          list={list}
          updateList={updateList}
        />
      </Modal>
      <TouchableOpacity
        style={[styles.listContainer, { backgroundColor: list.color }]}
        onPress={() => ToggleList()}
      >
        <Text style={styles.listTitle} numberOfLines={1}>
          {list.name}
        </Text>
        <View>
          <View style={{ alignItems: "center" }}>
            <Text style={styles.count}>{remainingCount}</Text>
            <Text style={styles.subtitle}>Remaining</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Text style={styles.count}>{completedCount}</Text>
            <Text style={styles.subtitle}>Completed</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default TodoList;

const styles = StyleSheet.create({
  listContainer: {
    paddingVertical: 32,
    paddingHorizontal: 16,
    borderRadius: 6,
    marginHorizontal: 12,
    alignItems: "center",
    width: 200,
  },
  listTitle: {
    fontSize: 24,
    fontWeight: "700",
    color: colors.white,
    marginBottom: 18,
  },
  count: {
    fontSize: 48,
    fontWeight: "200",
    color: colors.white,
  },
  subtitle: {
    fontSize: 12,
    fontWeight: "700",
    color: colors.white,
  },
});
